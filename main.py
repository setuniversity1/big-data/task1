import datetime

def main():
    now = datetime.datetime.now()
    print("Current time:", now.strftime("%H:%M:%S %d.%m.%Y"))
    print("Your first and last name:", "Andrii Shmelov")

if __name__ == "__main__":
    main()
