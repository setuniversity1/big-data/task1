---

## **Docker Introduction Task**

The main goal of this homework assignment is to familiarize yourself with the Docker platform. Throughout the course, you'll have the opportunity to develop various ideas and skills related to Docker.

### **Task Requirements:**

1. Prepare a program that will display on the screen:
    - Current time (hour, day, month, year)
    - Your name and surname
2. Prepare a Dockerfile from which you can build a container image. This container should package the code of the program. When the container is launched, the program's code should start running.
3. Prepare a shell script (**`build.sh`**) for building the image. As the evaluator of your work, I expect the image-building process to be executed by running the script you've prepared.

Your work will be considered complete if it includes:

- The program's code (you can choose the programming language)
- A correct Dockerfile
- A script (**`build.sh`**) containing the command to build the image from your Dockerfile
- A screenshot showing the successful execution of your container

Submit your work in the form of a link to a Merge Request on GitLab.

---
### **Step 1: Writing the Program**

1. Create a program that displays the following information on the screen:
    - Current time (hour, day, month, year)
    - Your name and surname
    
    ```bash
    nano main.py
    
    ```
    - add next Python script:
    
    ```python
    import datetime

    def main():
        now = datetime.datetime.now()
        print("Current time:", now.strftime("%H:%M:%S %d.%m.%Y"))
        print("Your first and last name:", "Andrea Esposito")

    if __name__ == "__main__":
        main()
    
    ```
    

### **Step 2: Dockerfile Preparation**

1. Write a Dockerfile to create a container image that packages your program's code. The Dockerfile should include instructions to run your program when the container starts.
    - create Dockerfile:
    ```bash
    nano main.py
    ```
    - add next:
    
    ```
    FROM python:3.10
    
    WORKDIR /app
    COPY main.py /app
    
    CMD ["python", "main.py"]
    
    ```
    

### **Step 3: Build Script Creation**

1. Prepare a shell script named **`build.sh`** to automate the image-building process.
Example **`build.sh`** script:
    
    ```bash
    #!/bin/bash
    
    docker build -t andrea .
    
    ```
    

### **Step 4: Building the Image and Running the Container**

1. Execute the **`build.sh`** script to build the Docker image using the Dockerfile.
    
    ```bash
    sh build.sh
    ```
    
2. Run the Docker image as a container:
    
    ```bash
    docker run -d andrea
    ```

### **Step 5: Verification and Submission**

1. Check your images:
    
    ```bash
    docker images    
    ```
2. Check your container:

    ```bash
    docker run -it andrea    
    ```
    
2. Capture a screenshot showing the successful execution of your container.

### **Step 6: Submission**

1. Submit your work as a link to a Merge Request on GitLab.